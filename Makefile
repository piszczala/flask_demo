# run_backend:
# 	docker-compose run web

build:
	docker-compose build --detach --force-recreate

run:
	docker-compose up -d

debug:
	docker attach flaskdemo_web_1

bash:
	docker-compose exec app bash

setup: build run