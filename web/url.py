from views import user_view, frontpage_view
from flask import Blueprint

template_page = Blueprint("template_page", __name__)


@template_page.route("/")
def rendered_frontpage():
    return frontpage_view()


@template_page.route("/<username>")
@template_page.route("/<username>/")
def user_profile_rendered(username):
    return user_view(username)
