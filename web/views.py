from image import get_image_urls_from_duckduckgo, get_latest_post
from flask import render_template


def user_view(user):
    avatar = get_image_urls_from_duckduckgo(f"{user} logo", 1)
    image_urls = get_image_urls_from_duckduckgo(user, 32)
    dump = {
        "avatar": avatar[0],
        "username": user,
        "photos": image_urls[1:],
    }
    return render_template("user.html", dump=dump)


def frontpage_view():
    latest_posts = [
        get_latest_post(user)
        for user
        in ["shreking", "tooploox", "bonusbgc_official", "steve_real_human"]
    ]
    return render_template("index.html", latest_posts=latest_posts)
