import itertools
import logging
import requests
from bs4 import BeautifulSoup
from duckduckgo_search import ddg_images
import random

REQUEST_HEADER = {
    "User-Agent": "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36"
}
GOOGLE_QUERY_URL = "https://www.google.co.in/search?q=%s&source=lnms&tbm=isch"


def get_image_urls_from_google(query, num_images=100):
    query = "_".join(query.split())
    url = GOOGLE_QUERY_URL % query
    response = requests.get(url, headers=REQUEST_HEADER)
    soup = BeautifulSoup(response.content, "html.parser")
    image_elements = soup.select("div a div img")
    image_urls = (image.get("src") for image in image_elements)
    return list(itertools.islice(image_urls, num_images))


def get_image_urls_from_duckduckgo(query, num_images=100):
    image_urls = (image["image"] for image in ddg_images(query))
    return list(itertools.islice(image_urls, num_images))


def get_latest_post(username):
    avatar = get_image_urls_from_duckduckgo(f"{username} logo", 1)
    image = get_image_urls_from_duckduckgo(username, 2)
    avatar = avatar[0] if avatar else image[0]
    return dict(author=username, avatar_url=avatar, photo_url=image[1], description='', likes=random.randint(0,100))