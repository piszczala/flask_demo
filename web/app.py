from flask import Flask, render_template
from url import template_page

app = Flask(__name__)
app.register_blueprint(template_page)


@app.route("/i/")
def frontpage():
    return f"Instagram board, new posts etc"


@app.route("/i/<string:username>")
def user_profile(username):
    return f"Information about user {username} with his posts etc"


@app.route("/i/<string:username>/saved/")
def profile_saved_posts(username):
    return f"Saved posts by user {username}"


@app.route("/i/<string:username>/tagged")
def profile_tagged_on_posts(username):
    return f"Posts where user {username} is tagged"


@app.route("/i/<string:username>/channel")
def profile_channel(username):
    return render_template("channel.html", username=username)
