# Flask Demo

## Url

https://wrocpy-demo.herokuapp.com/

## Description

Code to Wroc.py presentation

## Documentation

Flask na heroku:
https://medium.com/the-andela-way/deploying-a-python-flask-app-to-heroku-41250bda27d0

Gunicorn:
https://gunicorn.org/#quickstart

### Heroku

Gitlab CI -> variables -> HEROKU_API_KEY

https://dashboard.heroku.com/account sekcja API_KEY

### Procfile

To plik wykorzystywany przez Heroku do uruchamiania odpowiednich procesów.
https://devcenter.heroku.com/articles/procfile


